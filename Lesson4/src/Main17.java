public class Main17 {
    public static void main(String[] args) {
        double[] massiv = {1, 2, 3, 4, 6};
        double max = massiv[0];
        for (int i = 0; i < massiv.length; i++) {
            if (massiv[i] > max) max = massiv[i];
        }
        for (int i = 0; i < massiv.length; i++) {
            massiv[i] = massiv[i] / max;
        }
        for (int i = 0; i < massiv.length; i++) {
            System.out.println(massiv[i]);
        }

    }
}
