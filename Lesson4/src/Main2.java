public class Main2 {
    public static void main(String[] args) {
        int[] massiv = {1, 2, 3, 4, 6};
        int max = massiv[0];
        for (int i = 1; i < massiv.length; i++) {
            if(massiv[i] > max) max = massiv[i];
        }
        System.out.println("Наибольший элемент " + max);
    }
}
