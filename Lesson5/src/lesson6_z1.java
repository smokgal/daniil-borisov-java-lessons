import java.util.Random;
import java.util.Scanner;

public class lesson6_z1 {
    public static void main(String[] args) {
        int[][] massiv = new int[6][6];

        fillMassiv(massiv);
        printMassiv(massiv);
        sortMassiv(massiv);
        System.out.println();
        printMassiv(massiv);
    }

    static void fillMassiv(int[][] array) {
        Random random = new Random(8);
        //Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                //System.out.println("Please enter" + i + "," + j + "st element");
                array[i][j] = random.nextInt(50)+10;
            }

        }
    }

    static void printMassiv(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void sortMassiv(int[][] array) {
        int[] tmp = new int[array.length * array[0].length];
        int countOfTMP = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                tmp[countOfTMP] = array[i][j];
                countOfTMP++;
            }

        }
        int tmpArray = 0;
        for (int j = tmp.length - 1; j > 0; j--) {
            for (int i = 0; i < j; i++) {
                if (tmp[i] > tmp[i + 1]) {
                    tmpArray = tmp[i];
                    tmp[i] = tmp[i + 1];
                    tmp[i + 1] = tmpArray;

                }
            }
        }
        countOfTMP=0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = tmp[countOfTMP];
                countOfTMP++;
            }
        }
    }
}