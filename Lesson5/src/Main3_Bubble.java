import java.util.Arrays;

public class Main3_Bubble {
    public static void main(String[] args) {
        int [] massiv = {4,2,1,5,7,6,3,9,8};
        int tmp = 0;
        for (int j = massiv.length-1 ; j > 0;j--) {
            for (int i = 0; i < j; i++) {
                if (massiv[i] > massiv[i + 1]) {
                    tmp = massiv[i];
                    massiv[i] = massiv[i + 1];
                    massiv[i + 1] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(massiv));
    }

}
