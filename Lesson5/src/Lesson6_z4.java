import java.util.Random;

public class Lesson6_z4 {
    public static void main(String[] args) {
        int[][] data = new int[3][3];
        fillRandom(data, 5);
        System.out.println("Before");
        printArray(data);
        sord2d(data);
        System.out.println("After");
        printArray(data);
    }


    //сортировка
    static void sord2d(int[][] array) {
        for (int i = 0; i <array[0].length ; i++) {
            for (int j = 0; j <array[0].length-1 ; j++) {
                if (summElemsInColum(array, j) > summElemsInColum(array, j+1)){
                    cahngeColums(array,j,j+1);
                }

            }

        }
    }
    //сумма элементов в колонке
    static int summElemsInColum(int [][] array, int num) {
        int summ =0;
        for (int i = 0; i < array.length; i++) {
            summ += array[i][num];

        }
        return summ;
    }
    //замена колонок
    static  void cahngeColums(int[][] array, int x1, int x2){
        for (int i = 0; i <array.length ; i++) {
            int tmp = array[i][x1];
            array[i][x1] = array[i][x2];
            array[i][x2] = tmp;

        }
    }


    //печать двумерного массива
    static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

    }

    //заполняем двумерный массив случайными значениями до заданного диапазона
    static void fillRandom(int[][] array,int bound) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(bound);
            }
        }
    }
}
