public class Example {
    public static void main(String[] args) {
        int[] data = {1,3,5,4,8,6,3,5,4,7};
        printArray(data);
        arrayPlus(data);
        System.out.println();
        printArray(data);
        int b = 5;
        System.out.println(b);
        b = intPlus(b);
        System.out.println(b);

        int x1 = 5;
        int module = 3;
        int res = 1;
        for (int i = 0; i < module; i++) {
            res *=x1;
        }
        System.out.println(res);

    }

    static int intPlus(int a){
        return a + 1;
    }

    static void arrayPlus(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] + 2 * 3;
        }
    }

    static void printArray(int[] array){
        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
