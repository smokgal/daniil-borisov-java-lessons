public class Main5 {
    public static void main(String[] args) {
        double max = -50;
        double funct = Math.pow(max,3);
        for (double x = -50; x <=50 ; x+=0.01) {
            if (Math.pow (x,3) > Math.pow(max,3) ) {
                max=x;
                funct = Math.pow (x,3);
            }

        }
        System.out.println(max);
        System.out.println(funct);
    }
}
