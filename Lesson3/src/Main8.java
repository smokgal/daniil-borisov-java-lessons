public class Main8 {
    public static void main(String[] args) {
        double max = 0;
        double funct = Math.pow(Math.E,max);
        for (double x = -10; x < 10; x+=0.01) {
            double tmp = Math.pow(Math.E,x);
            if (tmp > funct) {
                max = x;
                funct = tmp;
            }

        }
        System.out.println(max);
        System.out.println(funct);
    }
}
