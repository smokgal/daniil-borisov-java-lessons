public class Main6 {
    public static void main(String[] args) {
        double max = -10;
        double funct = 1/ Math.pow(max,3);
        for (double x = -10; x<=10;  x+=0.01) {
            if (!(x > -1 && x < 1)) {
                double tmpF = 1 / Math.pow(x,3);
                if (tmpF > funct) {
                    max = x;
                    funct = tmpF;
                }
            }

        }
        System.out.println(max);
        System.out.println(funct);
    }
}
