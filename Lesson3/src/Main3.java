public class Main3 {
    public static void main(String[] args) {
        double max = -10;
        double funct = Math.sin(max) + Math.pow(max,2);
        for (double x = -10; x < 10 ; x+=0.01) {
            if (Math.sin(x) + Math.pow(x,2) > Math.sin(max) + Math.pow(max,2)) {
                max=x;
            }

        }
        System.out.println(max);
        System.out.println(funct);
    }
}
